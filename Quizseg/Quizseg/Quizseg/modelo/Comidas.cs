﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Quizseg.modelo
{
    class Comidas
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        [MaxLength(150)]
        public string nombre_comodas { get; set; }
    }
}
